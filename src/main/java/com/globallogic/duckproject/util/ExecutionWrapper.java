package com.globallogic.duckproject.util;

public class ExecutionWrapper {
	
	private ExecutionWrapper() {}
	
	public static void executeState(StateExecutor stateExecutor) {
		try {
			stateExecutor.execute();
		} catch (IllegalStateException ex) {
			ex.printStackTrace();
		}
	}
}
