package com.globallogic.duckproject.util;

public interface StateExecutor {
	public void execute() throws IllegalStateException;
}
