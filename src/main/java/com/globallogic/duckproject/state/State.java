package com.globallogic.duckproject.state;

import com.globallogic.duckproject.entity.Duck;

public interface State {
    default void fly(Duck duck) {
        throw new IllegalStateException();
    }
    default void swim(Duck duck) {
        throw new IllegalStateException();
    }
    default void walk(Duck duck) {
        throw new IllegalStateException();
    }
    default void run(Duck duck) {
        throw new IllegalStateException();
    }
    default void stand(Duck duck) {
        throw new IllegalStateException();
    }
    default void eat(Duck duck) {
        throw new IllegalStateException();
    }
    default void sleep(Duck duck) {
        throw new IllegalStateException();
    }
}
