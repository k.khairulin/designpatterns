package com.globallogic.duckproject.state;

import com.globallogic.duckproject.entity.Duck;

public class FlyingState implements State {

    @Override
    public void swim(Duck duck) {
        System.out.println("I am swimming!");
        duck.setState(StateFactory.SWIMMING_STATE);
    }

    @Override
    public void run(Duck duck) {
        System.out.println("I am running!");
        duck.setState(StateFactory.RUNNING_STATE);
    }

    @Override
    public void fly(Duck duck) {
        System.out.println("I am flying!");
    }
}
