package com.globallogic.duckproject.main;

import com.globallogic.duckproject.decorator.Chainlet;
import com.globallogic.duckproject.decorator.Label;
import com.globallogic.duckproject.decorator.Tag;
import com.globallogic.duckproject.entity.Duck;

public class App {
    public static void main(String[] args) {
        Duck duck1 = new Duck.DuckBuilder().age(3).name("Ducker").wingsLength(20.0).build();
        System.out.println(duck1.getDuckId());
        System.out.println(duck1.getColor());
        System.out.println(duck1.getWeight());
        System.out.println("===============================================================");
        Duck duck2 = new Duck.DuckBuilder().age(0).name("Ducker2").wingsLength(15.0).build();
        System.out.println(duck2.getDuckId());
        System.out.println(duck2.getColor());
        System.out.println(duck2.getWeight());
        System.out.println("===============================================================");
        Duck duck3 = new Duck.DuckBuilder().age(7).name("Ducker3").wingsLength(22.0).build();
        System.out.println(duck3.getDuckId());
        System.out.println(duck3.getColor());
        System.out.println(duck3.getWeight());
        System.out.println("===============================================================");
        System.out.println(duck1.getDuckId());
        System.out.println(duck2.getDuckId());
        System.out.println(duck3.getDuckId());
        System.out.println("===============================================================");
        Duck duck4 = duck1.getClone();
        System.out.println("duck1: "  + duck1);
        System.out.println("===============================================================");
        System.out.println("duck4: "  + duck4);
        System.out.println("STATE CHECK:===================================================");
        duck1.eat(); // eating
        duck1.fly(); // ex
        duck1.stand(); // stand
        duck1.stand(); // stand
        duck1.swim(); // swim
        duck1.walk(); // walk
        duck1.sleep(); // ex
        duck1.stand(); // stand
        duck1.sleep(); // sleep
        duck1.stand(); // stand
        duck1.fly(); // ex
        duck1.stand(); // stand
        duck1.walk(); // walk
        duck1.run(); // run
        duck1.fly(); // fly
        System.out.println("DECORATOR CHECK:===================================================");
        Label duckWithTag = new Tag(duck1);
        duckWithTag.showStatus();
        System.out.println("===============================================================");
        Label duckWithTagAndChainlet = new Chainlet(duckWithTag);
        duckWithTagAndChainlet.showStatus();
        System.out.println("===============================================================");
    }
}

