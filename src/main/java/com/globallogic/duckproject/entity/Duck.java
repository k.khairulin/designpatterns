package com.globallogic.duckproject.entity;

import com.globallogic.duckproject.decorator.Label;
import com.globallogic.duckproject.state.State;
import com.globallogic.duckproject.state.StateFactory;
import com.globallogic.duckproject.util.ExecutionWrapper;

public class Duck implements Cloneable, Label {
    private long duckId;
    private int age;
    private DuckColor color;
    private String name;
    private double weight;
    private double wingslength;
    private static long currentId;
    private State currentState;

    {
        currentId++;
    }

    public enum DuckColor {
        YELLOW, BROWN, GOLD, WHITE;
    }

    public static class DuckBuilder {
        private int age;
        private String name;
        private double wingslength;

        public DuckBuilder age(int val) {
            this.age = val;
            return this;
        }

        public DuckBuilder name(String val) {
            this.name = val;
            return this;
        }

        public DuckBuilder wingsLength(double val) {
            this.wingslength = val;
            return this;
        }

        public Duck build() {
            return new Duck(this);
        }

    }

    Duck(DuckBuilder builder) {
        this.duckId = currentId;
        this.age = builder.age;
        this.name = builder.name;
        this.wingslength = builder.wingslength;
        this.color = setColor(builder.age);
        this.weight = 4 * builder.wingslength;
        this.currentState = StateFactory.STANDING_STATE;
    }

    public Duck(Duck duck) {
        this.duckId = duck.duckId;
        this.age = duck.age;
        this.color = duck.color;
        this.name = duck.name;
        this.weight = duck.weight;
        this.wingslength = duck.wingslength;
        this.currentState = duck.currentState;
    }

    private DuckColor setColor(int age) {
        if (age < 1) {
            return DuckColor.YELLOW;
        } else if(age <= 5) {
            return DuckColor.BROWN;
        } else if(age <= 10) {
            return DuckColor.GOLD;
        } else {
            return DuckColor.WHITE;
        }
    }

    public long getDuckId() {
        return duckId;
    }

    public int getAge() {
        return age;
    }

    public DuckColor getColor() {
        return color;
    }

    public String getName() {
        return name;
    }

    public double getWeight() {
        return weight;
    }

    public double getWingslength() {
        return wingslength;
    }

    @Override
    protected Duck clone() throws CloneNotSupportedException {
        return (Duck) super.clone();
    }

    public Duck getClone() {
        Duck duck = null;
        try {
            duck =  this.clone();
        } catch (CloneNotSupportedException ex) {
            ex.printStackTrace();
        }
        return duck;
    }

    @Override
    public String toString() {
        return "Duck{" +
            "duckId=" + duckId +
            ", age=" + age +
            ", color=" + color +
            ", name='" + name + '\'' +
            ", weight=" + weight +
            ", wingslength=" + wingslength +
            '}';
    }

    public void setState(State state) {
        this.currentState = state;
    }

    public void fly() {
    	ExecutionWrapper.executeState(() -> currentState.fly(this));
    }

    public void swim() {
    	ExecutionWrapper.executeState(() -> currentState.swim(this));
    }

    public void walk() {
    	ExecutionWrapper.executeState(() -> currentState.walk(this));
    }

    public void run() {
    	ExecutionWrapper.executeState(() -> currentState.run(this));
    }

    public void stand() {
    	ExecutionWrapper.executeState(() -> currentState.stand(this));
    }

    public void eat() {
    	ExecutionWrapper.executeState(() -> currentState.eat(this));
    }

    public void sleep() {
    	ExecutionWrapper.executeState(() -> currentState.sleep(this));
    }

	@Override
	public void showStatus() {
		System.out.println("A duck. ");
	}
}